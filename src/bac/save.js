import React from 'react';
import { useBlockProps, RichText } from '@wordpress/block-editor';
import './style.scss'


export function Save(props){

    const { attributes } = props;

    return (
        <div { ...useBlockProps.save() } className="bac" style={{ background:attributes.background }}>
            <div className='bac__content'>
                <RichText.Content
                    value={attributes.title}
                    tagName="h1"
                />
                <RichText.Content
                    value={attributes.description}
                    tagName="p"
                />
                <div>
                    <a className='btn_primary' href={attributes.url ?? "#"}>
                        {attributes.label}
                    </a>
                </div>
            </div>
            <div className='bac__image'>
                <img src={attributes.imageUrl} alt={attributes.alt} />
            </div>
        </div>
    );
}